import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./index.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Jokes from "./components/Jokes";
import Header from "./components/Header";

ReactDOM.render(
  <div>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" render={() => <Header Component={App} />} />
        <Route path="/Jokes" render={() => <Header Component={Jokes} />} />
      </Switch>
    </BrowserRouter>
  </div>,
  document.getElementById("root")
);
