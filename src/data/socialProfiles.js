import emailIcon from "../assets/email_icon.png";
import gitHubIcon from "../assets/github_icon.png";
import linkedInIcon from "../assets/linkedin_icon.png";
import twitterIcon from "../assets/twitter_icon.png";

const SOCIAL_PROFILES = [
  {
    id: 1,
    link: "mailto:Jalyndboyd1@gmail.com",
    image: emailIcon,
  },
  {
    id: 2,
    link: "https://github.com/Jalyndboyd1",
    image: gitHubIcon,
  },
  {
    id: 3,
    link: "https://www.linkedin.com/in/jalyn-boyd-371931201/",
    image: linkedInIcon,
  },
  {
    id: 4,
    link: "https://twitter.com/JalynDBoyd1",
    image: twitterIcon,
  },
];

export default SOCIAL_PROFILES;
