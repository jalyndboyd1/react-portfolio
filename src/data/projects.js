import spotifyLogo from "../assets/spotify.png";
import project2 from "../assets/project2.png";
import airbnbLogo from "../assets/airbnb.jpg";
import tinderLogo from "../assets/tlogo.png";
import project1 from "../assets/project1.png";
import googleLogo from "../assets/googleL.png";

const PROJECTS = [
  {
    id: 1,
    title: "Music Master",
    description: "A React App I built using API resources from spotify!",
    link: "https://gitlab.com/jalyndboyd1/music-master.git",
    image: spotifyLogo,
  },
  {
    id: 2,
    title: "My API.",
    description:
      "A simple application that demonstrates simple uses of React. ",
    link: "https://gitlab.com/jalyndboyd1/todo-app-pt-1",
    image: project2,
  },
  {
    id: 3,
    title: "Air Bnb Clone",
    description: "Project that mimicks the functionality and style of air bnb.",
    link: "https://gitlab.com/jalyndboyd1/airbnb-clone.git",
    image: airbnbLogo,
  },
  {
    id: 4,
    title: "Tinder Clone",
    description:
      "Tinder clone meant to be viewed in inspector to simulate real phone experience .",
    link: "https://gitlab.com/jalyndboyd1/tinder-clone.git",
    image: tinderLogo,
  },
  {
    id: 5,
    title: "Photo Wall",
    description:
      "A satisfying API application to scroll through aesthetic photos.",
    link: "https://gitlab.com/jalyndboyd1/assessment---react-photo-wall.git",
    image: project1,
  },
  {
    id: 6,
    title: "Google Clone",
    description:
      "An application that has the same search functions and feel as the original google website!",
    link: "https://gitlab.com/jalyndboyd1/google-clone.git",
    image: googleLogo,
  },
];

export default PROJECTS;
