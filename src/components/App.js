import React, { Component } from "react";
import Projects from "./Projects";
import SocialProfiles from "./SocialProfiles";
import Profile_Pic from "../assets/Profile_Pic.jpg";
import Title from "./Title";
import Jokes from "./Jokes";

class App extends Component {
  state = { displayBio: false };

  toggleDisplayBio = () => {
    this.setState({ displayBio: !this.state.displayBio });
  };

  render() {
    return (
      <div>
        <h1>Hello!</h1>
        <img src={Profile_Pic} alt="Profile" className="profile" />
        <p>My name is Jalyn. I am a software engineer student!</p>
        <p>I am always looking forward to improving myself!</p>
        {this.state.displayBio ? (
          <div>{this.state.displayBio ? <Title /> : null}</div>
        ) : null}
        <div>
          <button onClick={this.toggleDisplayBio}>Read More</button>
        </div>
        <hr />
        <Projects />
        <hr />
        <SocialProfiles />
        <hr />
        <Jokes />
      </div>
    );
  }
}
export default App;
