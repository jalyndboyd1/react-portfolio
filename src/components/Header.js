import React from "react";
import { Link } from "react-router-dom";
//Navigation File
//Higher Order

function Header(props) {
  const { Component } = props;
  const style = {
    display: "inline-block",
    margin: 10,
    marginbottom: 30,
  };
  return (
    <div>
      <div>
        <h3 style={style}>
          <Link
            to="/"
            style={{
              textDecoration: "underline dotted",
              color: "pink",
            }}
          >
            Home
          </Link>
        </h3>
        <h3 style={style}>
          <Link
            to="/jokes"
            style={{
              textDecoration: "underline dotted",
              color: "pink",
            }}
          >
            Jokes
          </Link>
        </h3>
        <Component />
      </div>
    </div>
  );
}

export default Header;
