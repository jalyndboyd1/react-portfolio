import React from "react";
import SOCIAL_PROFILES from "../data/socialProfiles";

function SocialProfile(props) {
  const { link, image } = props.socialProfiles;
  return (
    <span>
      <a href={link}>
        <img
          src={image}
          alt="Social Media "
          style={{
            display: "inline-block",
            width: 35,
            height: 35,
            margin: 15,
          }}
        />
      </a>
    </span>
  );
}

function SocialProfiles(props) {
  return (
    <div>
      <h2>Connect with Me!</h2>
      <div>
        {SOCIAL_PROFILES.map((SOCIAL_PROFILES) => {
          return (
            <SocialProfile
              key={SOCIAL_PROFILES.id}
              socialProfiles={SOCIAL_PROFILES}
            />
          );
        })}
      </div>
    </div>
  );
}

export default SocialProfiles;
