import React from "react";
import PROJECTS from "../data/projects";

function Project(props) {
  const { title, description, image, link } = props.Project;
  return (
    <div style={{ display: "inline-block", width: 300, margin: 25 }}>
      <h3>{title}</h3>
      <img src={image} alt="Project" style={{ width: 200, height: 120 }} />
      <p>{description}</p>
      <a href={link}>{link}</a>
    </div>
  );
}

function Projects(props) {
  return (
    <div>
      <h2>Highlighted Projects</h2>
      <div>
        {PROJECTS.map((PROJECTS) => {
          return <Project key={PROJECTS.id} Project={PROJECTS} />;
        })}
      </div>
    </div>
  );
}

export default Projects;
